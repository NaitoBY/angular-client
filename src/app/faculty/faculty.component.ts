import { Component, OnInit } from '@angular/core';

import {Faculty} from "../model/faculty";
import {FacultyService} from "../service/faculty.service";

import {Person} from "../model/person";
import {PersonService} from "../service/person.service";

import {Filter} from "../model/filter";

@Component({
  selector: 'app-faculty',
  templateUrl: './faculty.component.html',
  styleUrls: ['./faculty.component.css']
})
export class FacultyComponent implements OnInit {
  model: Filter = new Filter();
  faculties:Faculty[] = [];
  faculty: Faculty = new Faculty();
  persons: Person[] = [];

 constructor(private facultyService: FacultyService, private personService: PersonService) { }

   ngOnInit(): void {
       this.facultyService.getFaculty().subscribe(
         (faculties) => {
           this.faculties = faculties;
         },
         err => {
           alert("error");
         }
       );
       this.personService.getPersons().subscribe(
         (persons) => {
           this.persons = persons;
         },
         err => {
           alert("error");
         }
       );
     }

     deleteFaculty(id:number) {
       this.facultyService.removeFaculty(id).subscribe(
         () => {
           window.location.reload();
         },
         (err) => {
           alert("error");
         }
       )
     }

     editFaculty(id: number) {
       document.getElementById("editFacultyForm")!.hidden=false;
       this.facultyService.getFacultyById(id).subscribe(
         faculty => {
           this.faculty = faculty;
         },
         err => {
           alert("error");
         }
       );
     }

     showCreateForm() {
       document.getElementById("createFacultyForm")!.hidden=false;
     }

     filterFaculty() {
       alert(this.model.id);
       this.facultyService.filterFaculty(this.model.id).subscribe(
         faculties => {
           this.faculties = faculties;
         },
         err => {
           alert("error");
         }
       );
     }
}
