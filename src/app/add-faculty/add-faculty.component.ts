import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

import {Faculty} from "../model/faculty";
import {FacultyService} from "../service/faculty.service"

import {Person} from "../model/person";
import {PersonService} from "../service/person.service"

@Component({
  selector: 'app-add-faculty',
  templateUrl: './add-faculty.component.html',
  styleUrls: ['./add-faculty.component.css']
})
export class AddFacultyComponent implements OnInit{
  model:Faculty = new Faculty();
  persons: Person[] = [];
  selectedPersons: Person[] =[];

   constructor(private personService: PersonService, private facultyService: FacultyService, private router: Router) { }


   ngOnInit(): void {
       this.personService.getPersons().subscribe(
         (persons) => {
           this.persons = persons;
         },
         err => {
           alert("error");
         }
       );
     }

     public savePersonToList(event) {
       let selectedPerson = this.persons.find(function(person:Person) {
         return person.id == event.target.value;
       });
       if(this.selectedPersons.indexOf(selectedPerson!) === -1) {
         this.selectedPersons.push(selectedPerson!);
       }
     }

     public removePersonFromList(person: Person) {
       this.selectedPersons.splice(this.selectedPersons.indexOf(person), 1);
     }

     public createFaculty() {
       this.model.persons = this.selectedPersons;
       this.facultyService.saveFaculty(this.model).subscribe(
         () => {
           window.location.reload();
         },
         (err) => {
           alert("error");
         }
       );
     }

}
