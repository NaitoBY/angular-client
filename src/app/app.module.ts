import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AddFacultyComponent } from './add-faculty/add-faculty.component';
import { EditFacultyComponent } from './edit-faculty/edit-faculty.component';
import { NavigationComponent } from './navigation/navigation.component';
import { FacultyComponent } from './faculty/faculty.component';

import {RouterModule, Routes} from "@angular/router";
import {HttpClientModule} from "@angular/common/http";
import {FormsModule} from "@angular/forms";


const appRoutes : Routes = [
  {
    path:"faculty",
    component:FacultyComponent
  },
  {
    path:"",
    pathMatch:"full",
    redirectTo:"faculty"
  }
  ];
@NgModule({
  declarations: [
   AppComponent,
   AddFacultyComponent,
   EditFacultyComponent,
   NavigationComponent,
   FacultyComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
