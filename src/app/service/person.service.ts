import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {Observable} from "rxjs";
import {Person} from "../model/person";

@Injectable({
  providedIn: 'root'
})
export class PersonService {
  readonly PERSON_URL = "http://localhost:8080/persons";

  constructor(private http: HttpClient, private router: Router) { }

  getPersons() : Observable<Person[]> {
    return this.http.get<Person[]>(this.PERSON_URL)
  }
}

