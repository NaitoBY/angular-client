import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Faculty} from "../model/faculty";
import {Observable} from "rxjs";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class FacultyService {

   readonly FACULTY_URL = "http://localhost:8080/faculty";
    readonly FILTER_URL = "http://localhost:8080/faculty/filter";

  constructor(private http: HttpClient, private router: Router) { }

    getFaculty() : Observable<Faculty[]> {
      return this.http.get<Faculty[]>(this.FACULTY_URL)
    }

    getFacultyById(id:number) : Observable<Faculty> {
      return this.http.get<Faculty>(this.FACULTY_URL + "/" + id);
    }

    saveFaculty(faculty:Faculty) : Observable<any> {
      return this.http.post(this.FACULTY_URL, faculty);
    }

    removeFaculty(id:number) : Observable<any> {
      return this.http.delete(this.FACULTY_URL + "/" + id);
    }

    updateFaculty(faculty: Faculty, id:number) : Observable<any> {
      return this.http.put(this.FACULTY_URL + "/" + id, faculty);
    }

    filterFaculty(id:number) : Observable<any> {
      return this.http.get(this.FILTER_URL + "?authorId=" + id);
    }
}
