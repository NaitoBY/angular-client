import {Person} from "./person";

export class Faculty {
  id!:number;
  nameFaculty!:string;
  phone!:string;
  persons:Person[] = [];
}
