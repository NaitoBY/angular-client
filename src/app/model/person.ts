import {Faculty} from "./faculty"

export class Person {
  id!:number;
  fio!:string;
  startWorking!:number;
  faculty:Faculty[] =[];
}
