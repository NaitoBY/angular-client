import { Component, OnInit,Input } from '@angular/core';
import {Faculty} from "../model/faculty";
import {Person} from "../model/person";
import {FacultyService} from "../service/faculty.service";

@Component({
  selector: 'app-edit-faculty',
  templateUrl: './edit-faculty.component.html',
  styleUrls: ['./edit-faculty.component.css']
})
export class EditFacultyComponent implements OnInit {
  @Input() faculty!: Faculty;
  @Input() persons: Person[] = [];
  @Input() selectedPersons: Person[] =[];

  constructor(private facultyService: FacultyService) {
    }

  ngOnInit(): void {
    // This is intentional
  }

    updateFaculty() {
      this.faculty.persons = this.selectedPersons;
      this.facultyService.updateFaculty(this.faculty, this.faculty.id).subscribe(
        () => {
          window.location.reload();
        },
        err => {
          console.log(err);
        }
      );
    }

    public savePersonToList(event) {
      let selectedPerson = this.persons.find(function(person:Person) {
        return person.id == event.target.value;
      });
      if(this.selectedPersons.indexOf(selectedPerson!) === -1) {
        this.selectedPersons.push(selectedPerson!);
      }
    }

    public removePersonFromList(person:Person) {
      this.selectedPersons.splice(this.selectedPersons.indexOf(person), 1);
    }

}
